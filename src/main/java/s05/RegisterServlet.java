package s05;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discover = req.getParameter("discover_type");
		String birthday = req.getParameter("birthday");
		String type = req.getParameter("type");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession();
		
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discover", discover);
		session.setAttribute("birthday", birthday);
		session.setAttribute("type", type);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
		


	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}


}

