<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Confirm Information</title>
	</head>
	<body>
		<h1>Registration confirmation</h1>
		<p>First Name: <%= session.getAttribute("firstName") %></p>
		<p>Last Name: <%= session.getAttribute("lastName") %></p>
		<p>Phone: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= session.getAttribute("discover") %></p>
		<p>Date of Birth: <%= session.getAttribute("birthday") %></p>
		<p>User Type: <%= session.getAttribute("type") %></p>
		<p>Description: <%= session.getAttribute("description") %></p>
		
		<form action="login" method="post">
			<input type="submit">
		</form>
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
		
	</body>	
</html>