<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Job Finder</title>
	</head>
	<body>
		<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="post">
			<div>
				<label for=fristName>First Name: </label>
				<input type="text" name=firstName required/>	
			</div>
			<div>
				<label for=lastName>Last Name: </label>
				<input type="text" name=lastName required/>
			</div>
			<div>
				<label for=phone>Phone: </label>
				<input type="text" name=phone required/>
			</div>
			<div>
				<label for=email>Email: </label>
				<input type="email" name=email required/>
			</div>
	
			<fieldset>
				<legend>How did you discover the app?</legend>
				<input type="radio" id="friends" name="discover_type" value="Friends" required>
				<label for="friends">Friends</label> <br>
				<input type="radio" id="social_media" name="discover_type" value="Social Media" required>
				<label for="socail_media">Social Media</label> <br>
				<input type="radio" id="others" name="discover_type" value="Others" required>
				<label for="others">Others</label>
			</fieldset>
			<div>
				<label for="birthday">Date of birth: </label>
				<input type="date" name="birthday" required>
			</div>
			<div>
				<label for="type">Are you an employer or applicant?</label>
					<select id="type_person" name="type">
						<option value="" selected="selected">Select One</option>
						<option value="applicant">Applicant</option>
						<option value="employer">Employer</option>
					</select>
			</div>
			<div>
				<label for="description">Profile Description: </label>
				<textarea name="description" maxlength="500"></textarea>
			</div>
			
			<button>Register</button>
		</form>
	</body> 
</html>